Future<String> createOrderMessage() async {
  var order = fetchUserOrder();
  return 'Your order is : $order';
}

Future<String> fetchUserOrder() => Future.delayed(
    const Duration(seconds: 2),
    () => 'Large Latte',
  ); //delay
void main() async{
  print('Fetching user order. . .');
  print(createOrderMessage());
  print('End of create order message');
}