class Dog {
   int id;
   String name;
   int age;

  Dog({
    required this.id,
    required this.name,
    required this.age,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'age': age,
    };
  }

  static List<Dog> toList(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (i) {
      return Dog(id: maps[i]['id'], name: maps[i]['name'], age: maps[i]['age']);
    });
  }

  @override
  String toString() {
    return 'Dog (id: $id, name: $name, age: $age)';
  }
}
