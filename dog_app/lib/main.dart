import 'package:dog_app/dog.dart';
import 'package:dog_app/dog_service.dart';
import 'package:flutter/material.dart';

import 'dog_app_widget.dart';

void main() {
  runApp(MaterialApp(
    title: 'Dog App',
    home: DogApp(),
  ));
}
